*React, *Redux, Node.js, Express, Mongodb, Webpack.

---
 Goal of project:

 * Make an Authentication with token.



Heroku repo: https://git.heroku.com/sing-song.git








Usage
---

Start the development server with this command:

```
npm start
```



Setup
---

```
npm install
```



Compile
---

```
npm run compile




**** Handy articles ***

 https://www.tutorialspoint.com/reactjs/reactjs_component_life_cycle.htm

 https://daveceddia.com/ajax-requests-in-react/

https://www.lynda.com/React-js-tutorials/Authentication-setup/558648/597687-4.html?srchtrk=index%3a1%0alinktypeid%3a2%0aq%3aauthentication+react%0apage%3a1%0as%3arelevance%0asa%3atrue%0aproducttypeid%3a2

https://davidwalsh.name/react-authentication

https://developer.okta.com/blog/2017/03/30/react-okta-sign-in-widget

https://auth0.com/blog/adding-authentication-to-your-react-flux-app/



https://scotch.io/tutorials/authenticate-a-node-js-api-with-json-web-tokens


https://medium.com/ecmastack/uploading-files-with-react-js-and-node-js-e7e6b707f4ef


IndexedDB:

https://www.tutorialspoint.com/html5/html5_indexeddb.htm

https://habrahabr.ru/post/213515/

https://www.tutorialspoint.com/html5/html5_indexeddb.htm

http://dexie.org/

FLEX: https://css-tricks.com/almanac/properties/j/justify-content/


Offline first:

https://github.com/pazguille/offline-first

Heroku:

"postinstall": "webpack -p --config ./webpack.prod.config.js --progress"

In this approach make sure to?heroku config:set NODE_ENV=production?heroku config:set NPM_CONFIG_PRODUCTION=true

OR

You can find this?custom buildpack?heroku-buildpack-webpack?usable.

* These links might help you build understanding:

?	heroku hook-things-up
?	npm scripts

https://devcenter.heroku.com/articles/node-best-practices#hook-things-up

https://medium.com/@addyosmani/progressive-web-apps-with-react-js-part-3-offline-support-and-network-resilience-c84db889162c

https://github.com/jerrysu/heroku-buildpack-webpack


REDUX-FORM:

https://www.npmjs.com/package/redux-form

https://medium.com/dailyjs/why-build-your-forms-with-redux-form-bcacbedc9e8


https://www.youtube.com/watch?v=6C22gwvOzaw



# create-react-app with a Node server on Heroku

Guide:

https://github.com/tublitzed/webpack2-express-heroku-starter/



A minimal example of using a Node backend (server for API, proxy, & routing) with a [React frontend](https://...).


## Demo

[Demo deployment]( https://warm-badlands-44519.herokuapp.com):  https://warm-badlands-44519.herokuapp.com


## Deploy to Heroku

```bash
git clone https://svetlana_khmel@bitbucket.org/svetlana_khmel/heroku-webpack-react-lyrics.git
cd heroku-webpack-react-lyrics/
heroku create
git push heroku master
```

In a separate terminal from the API server, start the UI:

```bash

# Initial setup
npm install

# Start the server
npm start
```



