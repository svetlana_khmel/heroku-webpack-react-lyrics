var mongoose = require('mongoose');
var User = require('./user');

// var Message = mongoose.model('Message',{
//     message: {}
// });
//
// module.exports = Message;

module.exports = mongoose.model('Message', {
    data: {},
    user: {
        type: mongoose.Schema.ObjectId,
        ref: 'User'
    }
});