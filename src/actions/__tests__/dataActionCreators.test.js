import * as actions from '../dataActionCreators';
import * as types from '../../actionTypes';

import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';
import expect from 'expect' // You can use any testing library

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

//[{"_id":"59ecee62f4b1b215d054ac0b","data":{"title":"Your Heart Is As Black As Night","article":[],"translation":[],"category":"Melody Gardot"},"user":{"_id":"59ece5a0f4b1b215d054ac09","username":"test4","password":"86985e105f79b95d6bc918fb45ec7727","__v":0},"__v":0},{"_id":"5a0c458b12407a98d9ce5c10","data":{"title":"0004545","article":"0000","translation":[],"category":"","articleId":"5a0c458b12407a98d9ce5c10"},"user":{"_id":"59ece5a0f4b1b215d054ac09","username":"test4","password":"86985e105f79b95d6bc918fb45ec7727","__v":0},"__v":0}]

describe('actions', () => {
    it('should create an action get messages', () => {
        const payload = 'Finish docs';
        const expectedAction = {
            type: types.GET_MESSAGES,
            payload
        };
        expect(actions.getDataCreator(payload)).toEqual(expectedAction)
    })
});

describe('actions', () => {
    it('should create an action to add a todo', () => {
        const payload = {"title":"Your Heart Is As Black As Night","article":[],"translation":[],"category":"Melody Gardot"};
        const expectedAction = {
            type: types.ADD_ARTICLE,
            payload
        };
        expect(actions.addArticleCreator(payload)).toEqual(expectedAction)
    })
});

describe('actions', () => {
    it('should create an action to remove a todo', () => {
        const id = '5a10c07f1f1b8aade6c6ba95';
        const expectedAction = {
            type: types.REMOVE_ARTICLE,
            id
        };
        expect(actions.removeArticle(id)).toEqual(expectedAction)
    })
});

describe('actions', () => {
    it('should create an action to toggle status of todo', () => {
        const id = '1';
        const expectedAction = {
            type: types.TOGGLE_STATUS,
            id
        };
        expect(actions.toggleStatus(id)).toEqual(expectedAction)
    })
});

describe('actions', () => {
    it('should create an action to filter todos', () => {
        const payload = 'Finish docs';
        const expectedAction = {
            type: types.FILTER_ARRAY,
            payload
        };
        expect(actions.applyTodoFilter(payload)).toEqual(expectedAction)
    })
});

describe('async actions', () => {
    afterEach(() => {
        fetchMock.reset();
        fetchMock.restore()
    });

    it('creates FETCH_TODOS_SUCCESS when fetching todos has been done', () => {
        fetchMock
            .getOnce('/json', { body: [
                    {"_id":"59ecee62f4b1b215d054ac0b",
                        "data":{
                            "title":"Your Heart Is As Black As Night",
                            "article":[],
                            "translation":[],
                            "category":"Melody Gardot"},
                        "user":{"_id":"59ece5a0f4b1b215d054ac09",
                            "username":"test4",
                            "password":"86985e105f79b95d6bc918fb45ec7727",
                            "__v":0
                        },"__v":0},
                {"_id":"5a0c458b12407a98d9ce5c10",
                    "data":{
                    "title":"0004545",
                        "article":"0000",
                        "translation":[],
                        "category":"",
                        "articleId":"5a0c458b12407a98d9ce5c10"
                    },"user":{
                    "_id":"59ece5a0f4b1b215d054ac09",
                    "username":"test4",
                    "password":"86985e105f79b95d6bc918fb45ec7727",
                    "__v":0},
                    "__v":0}
                ]
                , headers: { 'content-type': 'application/json' } });

        const expectedActions = [
            { type: types.GET_MESSAGES, payload: [
                {"_id":"59ecee62f4b1b215d054ac0b",
                    "data":{
                        "title":"Your Heart Is As Black As Night",
                        "article":[],
                        "translation":[],
                        "category":"Melody Gardot"},
                    "user":{"_id":"59ece5a0f4b1b215d054ac09",
                        "username":"test4",
                        "password":"86985e105f79b95d6bc918fb45ec7727",
                        "__v":0
                    },"__v":0},
                {"_id":"5a0c458b12407a98d9ce5c10",
                    "data":{
                        "title":"0004545",
                        "article":"0000",
                        "translation":[],
                        "category":"",
                        "articleId":"5a0c458b12407a98d9ce5c10"
                    },"user":{
                    "_id":"59ece5a0f4b1b215d054ac09",
                    "username":"test4",
                    "password":"86985e105f79b95d6bc918fb45ec7727",
                    "__v":0},
                    "__v":0}
            ]
            }
        ];
        const store = mockStore([]);

        return store.dispatch(actions.loadData()).then(() => {

            // return of async actions
            expect(store.getActions()).toEqual(expectedActions)
        })
    })
});





