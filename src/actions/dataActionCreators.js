import data from '../data/data';
import * as types from '../actionTypes';

export const getDataCreator = (data) => {
    if(typeof Storage !== 'undefined') {
        localStorage.setItem('lyricsData', JSON.stringify(data));
    }
    return {
        type: types.GET_MESSAGES,
        payload: data
    }
};

export const addArticleCreator = (data) => {
    debugger;
    return {
        type: types.ADD_ARTICLE,
        payload: data
    }
};

export const removeArticle = (id) => {
    return {
        type: types.REMOVE_ARTICLE,
        id
    }
};

export const editArticleCreator = (data) => {
    return {
        type: types.EDIT_ARTICLE,
        data
    }
};

export const setUser = (data) => {
    return {
        type: types.LOGIN_USER,
        payload: data
    }
};

export const logOutCreator = () => {
    return {
        type: types.LOGOUT
    }
};

export const createUser = (data) => {
    return {
        type: types.REGISTER_USER,
        payload: data
    }
};

export const loadData = (token) => {
    return (dispatch, getState) => {
        return data.getMessages(token)
            .then((response)=>response.json())
            .then((data)=> dispatch(getDataCreator(data)))
            .catch((err)=> console.log(err));
    };
};

export const loginUser = (userdata) => {
    return (dispatch) => {
        return data.loginUser(userdata)
            .then((response)=>response.json())
            .then((data)=> {
                console.log('ddd', data);
                dispatch(setUser(data));
            })
            .catch((err)=> console.log(err));
    };
};

export const registerUser = (userdata) => {
    return (dispatch) => {
        return data.registerUser(userdata)
            .then((response)=>response.json())
            .then((data)=> dispatch(createUser(data)))
            .catch((err)=> console.log(err));
    };
};

export const deleteArticle = (id) => {
    return (dispatch) => {
        return data.deleteArticle(id)
            .then((response)=>response.json())
            .then((data)=> dispatch(removeArticle(id)))
            .catch((err)=> console.log(err));
    };
};

export const editArticle = (token, id, article) => {
    return (dispatch) => {
        return data.editArticle(token, id, article)
            .then((response)=>response.json())
            .then((data)=> dispatch(editArticleCreator(data)))
            .catch((err)=> console.log(err));
    };
};

export const addArticle = (token, id, article) => {
    return (dispatch) => {
        return data.addArticle(token, id, article)
            .then((response)=>response.json())
            .then((data)=> dispatch(addArticleCreator(data)))
            .catch((err)=> console.log(err));
    };
};













