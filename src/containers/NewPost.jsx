import React, { Component } from 'react';
import ArticleForm from '../components/articleForm';
import Navigation from '../components/navigation';
import Header from './Header';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../actions/dataActionCreators';

class NewPost extends Component {
    constructor (props) {
        super(props);

        this.sendData = this.sendData.bind(this);
        this.getInputData = this.getInputData.bind(this);
        this.getSessionToken = this.getSessionToken.bind(this);

        this.state = {
            fullArticle: {
                title: '',
                articleRaw: '',
                article: [],
                translation: [],
                category: ''
            }
        }
    }

    getSessionToken () {
        if (typeof(Storage) !== "undefined") {
            return sessionStorage.getItem("access_token");
        } else {
            // Sorry! No Web Storage support..
        }
    }

    componentWillReceiveProps (nextProps) {
        if (this.props.data.length < nextProps.data.length) {
            this.setState({
                fullArticle: {
                    title:   "",
                    article: [],
                    translation: [],
                    category: ''
                }
            });

            this.props.history.push('/list');
        }
    }

    sendData () {
        let token = this.getSessionToken();

        let data = {
            title: this.state.fullArticle.title,
            article: /\n/.test(this.state.fullArticle.article) ? this.state.fullArticle.article.split(/\n/) : this.state.fullArticle.article,
            translation: /\n/.test(this.state.fullArticle.translation)? this.state.fullArticle.translation.split(/\n/) : this.state.fullArticle.translation,
            category: this.state.fullArticle.category
        };

        this.props.addArticle(token, data);
    }

    getInputData (event) {
        let title = "";
        let article = [];
        let translation = [];
        let category = '';
        let name = event.target.dataset.attr;

        if(name === 'title') {
            title = event.target.value;
            article = this.state.fullArticle.article;
            translation = this.state.fullArticle.translation;
            category = this.state.fullArticle.category;
        }

        if(name === 'article') {
            console.log('event.target.value', event.target.value);

            let rawData = event.target.value;

            //article = rawData.split(/\n/);
            article = rawData;
            title = this.state.fullArticle.title;
            translation = this.state.fullArticle.translation;
            category = this.state.fullArticle.category;
        }

        if(name === 'translation') {
            console.log('event.target.value', event.target.value);

            let rawData = event.target.value;

            //translation = rawData.split(/\n/);
            translation = rawData;

            title = this.state.fullArticle.title;
            article = this.state.fullArticle.article;
            category = this.state.fullArticle.category;
        }

        if(name === 'category') {

            category = event.target.value;

            title = this.state.fullArticle.title;
            article = this.state.fullArticle.article;
            translation = this.state.fullArticle.translation;
        }

        this.setState({
            fullArticle: {
                title,
                article,
                translation,
                category
            }
        });
    }

    render () {
        return (
            <div>
                <Header history={this.props.history} />
                <Navigation/>
                <ArticleForm sendData={this.sendData} translationvalue={this.state.fullArticle.translation} articlevalue={this.state.fullArticle.article} articleattr="article" titlevalue={this.state.fullArticle.title} titleattr="title"  getInputData={this.getInputData}/>
            </div>)
    }
}

const mapStateToProps = (state) => {
    return {
        data: state.data
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({addArticle: actions.addArticle}, dispatch);
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NewPost);