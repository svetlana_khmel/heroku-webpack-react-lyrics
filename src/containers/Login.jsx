import React, {Component} from 'react';
import Login from '../components/login';
import Register from '../components/register';

import SyncValidationForm from '../components/redux-form/SyncValidationForm';
import SyncValidationRegistrationForm from '../components/redux-form/SyncValidationRegistrationForm';

import { reduxForm } from 'redux-form';

import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as actions from "../actions/dataActionCreators";

import md5 from 'md5';

class App extends Component {
    constructor(props) {
        super(props);

        this.handleLogin = this.handleLogin.bind(this);
        this.handleRegister = this.handleRegister.bind(this);
        this.handleFormChange = this.handleFormChange.bind(this);
        this.loginUser = this.loginUser.bind(this);
        this.storeSession = this.storeSession.bind(this);
        this.getSessionToken = this.getSessionToken.bind(this);
        this.setLoginedMode = this.setLoginedMode.bind(this);

        this.state = {
            loginUsername: '',
            loginEmail: '',
            loginPassword: '',
            registerUsername: '',
            registerEmail: '',
            registerPassword: '',
            registerPasswordConfirm: '',
            passwordConformation: true,
            registerPasswordNegativeMesage: 'Passwords don\'t match',
            error: '',
            isLoggedIn: false
        }
    }

    componentWillReceiveProps(nextProps) {
        const error = nextProps.user.error;

        if (nextProps.user && Object.keys(nextProps.user).length !== 0 && !error ) {
            const payload = nextProps.user;
            this.setLoginedMode(payload);
        }

        if (nextProps.user.error){
            this.setState({
                error
            })
        }
    }

    componentDidMount() {
        let token = this.getSessionToken();

        if (token && token !== null) {
            this.setState({
                isLoggedIn: true
            });

            this.props.history.push('/list');
        }
    }

    setLoginedMode (payload) {
        if (payload) {
            this.storeSession(payload.token);

            if (payload.errors) {
                this.setState({
                    error: payload.errors.username.message
                });
            }
        }

        this.setState({
            isLoggedIn: true
        });

        this.props.history.push('/list');
    }

    registerUser() {
        const {username, password} = this.props.form.SyncValidationRegistrationForm.values;

        const data = {
            username,
            password: md5(password)
        };

        this.props.registerUser(data);
    }

    loginUser() {
        const {username, password} = this.props.form.syncValidation.values;

        const data = {
            username,
            password: md5(password)
        };

        console.log('Handle login.... ', this.props);

        this.props.loginUser(data);
    }

    storeSession(token) {
        if (typeof(Storage) !== "undefined") {
            sessionStorage.setItem("access_token", token);
        } else {
            // Sorry! No Web Storage support..
        }
    }

    getSessionToken() {
        if (typeof(Storage) !== "undefined") {
            return sessionStorage.getItem("access_token");
        } else {
            // Sorry! No Web Storage support..
        }
    }

    handleFormChange(event) {
        let data = {};

        data[event.target.dataset.attr] = event.target.value;

        this.setState(
            data
        );
    }

    handleLogin(e) {
        e.preventDefault();
        this.loginUser();
    }

    handleRegister(e) {
        e.preventDefault();

        this.registerUser();
    }

    render() {
        const isLoggedIn = this.state.isLoggedIn;

        return (
            <div className="messenger-container">

                {isLoggedIn == false &&

                    <div className="auth-block">
                        <div className="auth-error">{this.state.error}</div>
                        <div className="register-block"><SyncValidationRegistrationForm handleSubmit={this.handleRegister}/></div>
                        <div className="login-block"><SyncValidationForm handleSubmit={this.handleLogin}/></div>
                    </div>
                }

            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
        form: state.form ? state.form : ''
    };
};

export default connect(
    mapStateToProps,
    dispatch => bindActionCreators(actions, dispatch)
)(App);
