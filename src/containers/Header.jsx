import React, { Component } from 'react';

import {connect} from 'react-redux';
import {logOutCreator} from '../actions/dataActionCreators';

class Header extends Component {
    constructor (props) {
        super(props);

        this.logout = this.logout.bind(this);
    }

    logout () {
        this.setState({
            isLoggedIn: false
        });

        sessionStorage.removeItem('access_token');
        this.props.history.push('/login');
        this.props.logOutCreator();
    }

    render () {
        return (
            <div className="header">
                <a className="logout-link btn" onClick={this.logout}>Logout</a>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        data: state.data,
        user: state.user
    };
};

const mapDispatchToProps = dispatch => {
    return {
        logOutCreator: () => {
            dispatch(logOutCreator())
        }
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Header);
