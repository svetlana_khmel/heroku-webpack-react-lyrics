import reducer from '../dataReducer';
import * as types from '../../actionTypes';

describe('data reducer', () => {

    let initialState = [];

    it('should return the initial state', () => {
        expect(reducer(undefined, [])).toEqual(initialState)
    });

    it('Should handle GET_MESSAGES', () => {
        expect(
            reducer([], {
                type: types.GET_MESSAGES,
                payload: {
                    "title":"Your Heart Is As Black As Night",
                    "article":[],
                    "translation":[],
                    "category":"Melody Gardot"
                }
            })
        ).toEqual([{
            "title":"Your Heart Is As Black As Night",
            "article":[],
            "translation":[],
            "category":"Melody Gardot"
        }]);
    });

    it('should handle ADD_ARTICLE', () => {
        expect(
            reducer([], {
                type: types.ADD_ARTICLE,
                payload: {
                    "title":"Your Heart Is As Black As Night",
                    "article":[],
                    "translation":[],
                    "category":"Melody Gardot"
                }
            })
        ).toEqual([{
            "title":"Your Heart Is As Black As Night",
            "article":[],
            "translation":[],
            "category":"Melody Gardot"
        }]);

        expect(
            reducer(
                [
                    {
                        "title":"Your Heart Is As Black As Night",
                        "article":[],
                        "translation":[],
                        "category":"Melody Gardot"
                    }
                ],
                {
                    type: types.ADD_ARTICLE,
                    payload: {
                        "title":"Test",
                        "article":[],
                        "translation":[],
                        "category":"Test"
                    }
                }
            )
        ).toEqual([
            {
                "title":"Your Heart Is As Black As Night",
                "article":[],
                "translation":[],
                "category":"Melody Gardot"
            },
            {
                "title":"Test",
                "article":[],
                "translation":[],
                "category":"Test"
            }
        ])
    })
});