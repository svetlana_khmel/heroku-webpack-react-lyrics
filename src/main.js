import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/App';
import About from './containers/About';
import NewPost from './containers/NewPost';
import List from './containers/List';
import Login from './containers/Login';

//import { Router, Route, browserHistory } from 'react-router'
//import { BrowserRouter, Route } from 'react-router-dom';

import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom';

import { Provider } from 'react-redux';
import configureStore from './configureStore';

const store = configureStore();

ReactDOM.render(
    <Provider store={store}>
        <Router history={ history }>
            <div>
                <Route path='/' component={App} />
                <Route path='/about' component={About}/>
                <Route path='/login' component={Login}/>
                <Route path='/new' component={NewPost}/>
                <Route path='/list' component={List}/>
            </div>
        </Router>
    </Provider>,
    document.getElementById('mount')
);
