import React, { PropTypes } from 'react'

const Header = ({handleFormChange, passwordConformation, registerPasswordNegativeMesage, handleRegister}) => {

    return (
        <div className="register-block">
            <h1>Register</h1>
            <form>
                <div className="form-group">
                    <label htmlFor="username">Username:</label>
                    <input type="text" data-attr="registerUsername" className="text-input form-control" id="username"
                           onKeyUp={handleFormChange} onChange={handleFormChange}/>
                </div>
                {/*<div className="form-group">*/}
                {/*<label htmlFor="email">Email address:</label>*/}
                {/*<input type="email" data-attr="registerEmail" className="form-control" id="email" onKeyUp={this.handleFormChange} />*/}
                {/*</div>*/}

                <div className="form-group">
                    <label htmlFor="pwd">Password:</label>
                    <input type="password" data-attr="registerPassword" className="text-input form-control" id="pwd"
                           onKeyUp={handleFormChange} onChange={handleFormChange}/>
                </div>

                <div className="form-group">
                    <label htmlFor="pwd">Password confirm:</label>
                    <input type="password" data-attr="registerPasswordConfirm" className="text-input form-control"
                           id="pwdConfirm" onKeyUp={handleFormChange} onChange={handleFormChange}/>
                </div>

                {passwordConformation == false &&
                    <div className="tooltip">
                        {registerPasswordNegativeMesage}
                    </div>
                }

                {/*<div className="checkbox">*/}
                {/*<label><input type="checkbox"/> Remember me</label>*/}
                {/*</div>*/}
                <button type="button" className="btn btn-default" onClick={handleRegister}>Submit
                </button>
            </form>
        </div>
    );
};

Header.propTypes = {
    handleFormChange: PropTypes.func,
    passwordConformation: PropTypes.func,
    registerPasswordNegativeMesage: PropTypes.string,
    handleRegister: PropTypes.func,
};

export default Header;

