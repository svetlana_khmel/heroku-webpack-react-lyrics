import React from 'react';

const renderField = ({ input, label, type, meta: { touched, error, warning } }) => (
    <div className={'form-group'}>
        <label>{label}</label>
        <div>
            <input {...input} placeholder={label} type={type} className={'text-input'}/>
            {touched && ((error && <span className={'error'}>{error}</span>) || (warning && <span className={'warn'}>{warning}</span>))}
        </div>
    </div>
);

export default renderField;