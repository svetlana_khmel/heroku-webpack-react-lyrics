import React from 'react';
import {Link} from 'react-router-dom';

const Navigation = (props) => {
    const active = window.location.pathname.slice(1);

    return (
        <div>
            <ul className={'nav-menu'}>
                <li className={(active === 'list'?'active': '**') + ' nav-item'}><Link to="/list">List</Link></li>
                <li className={(active === 'about'?'active': '') + ' nav-item'}><Link to="/about">About</Link></li>

                {navigator.onLine === true &&
                    <li className={(active === 'new'?'active': '') + ' nav-item'}><Link to="/new">New</Link></li>
                }
            </ul>
        </div>
    )
};

export default Navigation;