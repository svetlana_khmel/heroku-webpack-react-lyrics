import React, { Component } from 'react';

class Category extends Component {
    constructor (props) {
        super(props);

        this.showCategory = this.showCategory.bind(this);
    }

    showCategory (e) {
        console.log(e.target);
        this.props.showCategory(e.target.dataset.category);
    }

    render () {
        let categories = [];

        {  this.props.messages != 0 &&

            this.props.messages.map((el) => {

                if (!categories.includes(el.data.category)) {

                    if(/,/.test(el.data.category)) {
                        let arr = el.data.category.split(",");

                        arr.forEach((element) => {
                            if (! categories.includes(element)) {
                                categories.push(element);
                            }
                        });

                    } else {
                        categories.push(el.data.category);
                    }
                }
            });
        }

        return (<div className="filter-block">

            <div className="filters">
                {  <div data-category='all' className="category-item filter" onClick={this.showCategory}>All</div> }

                { categories.length != 0 &&
                    categories.map((el, index)=>{
                        return <div key={el + index} data-category={el} className="category-item filter" onClick={this.showCategory}>{el}</div>
                    })
                }
            </div>

        </div>)
    }
}

export default Category;