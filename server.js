const express = require('express');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpack = require('webpack');
const webpackConfig = require('./webpack.config.js');
const app = express();

var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var Message = require('./model/message');
var User = require('./model/user');

var jwt    = require('jsonwebtoken');
var morgan = require('morgan');
var config = require('./config');


// get an instance of the router for api routes
var apiRoutes = express.Router();

app.set('superSecret', config.secret); // secret variable

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
// use morgan to log requests to the console
app.use(morgan('dev'));

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Content-Type, Authorization");
    next();
});

// route middleware to verify a token
apiRoutes.use(function(req, res, next) {

    // check header or url parameters or post parameters for token
    var token = req.body.token || req.query.token || req.headers['x-access-token'];

    // decode token
    if (token) {

        // verifies secret and checks exp
        jwt.verify(token, app.get('superSecret'), function(err, decoded) {
            if (err) {
                return res.json({ success: false, message: 'Failed to authenticate token.' });
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;
                req.user = decoded.user;
                next();
            }
        });

    } else {

        // if there is no token
        // return an error
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        });
    }
});

// app.get('/*', (req, res) => {
//     res.sendFile(path.join(__dirname, 'index.html'))
// });

app.get('/list', (req, res) => {
    res.redirect('/');
});

app.get('/login', (req, res) => {
    res.redirect('/');
});

app.get('/new', (req, res) => {
    res.redirect('/#/new');
});

app.post('/api/message', apiRoutes, function(req,res){
    console.log("Message_ _++++++ _ ______/api/message", req.body);
    req.body.user = req.user;

    var message = new Message(req.body);

    console.log(". . . .. . . . . ", message);

    message.save(function (err, product, numAffected) {
       // console.log(". . . .. . . . . ", message);
        res.json(message);

    });
    //res.send('200');

    //getMessages(req, res);
    //res.status(200);
    //getMessages (req, res);
});

app.post('/api/messages', apiRoutes, function (req, res, next) {
    console.log('........req.body ++++++', req.body);
    console.log('........  req.user  >>>>>>      ', req.user);

    // Message.find({}, function (err, result) {
    //     if (err) return next(err);
    //
    //     res.json(result);
    // });

    Message.find({user: req.user._id}).populate('user', '-pwd').exec(function (err, result) {
        if (err) return next(err);

        res.json(result);
    });
});

function getMessages (req, res) {
    Message.find({}, function (err, result) {
        if (err) return next(err);

        res.json(result);
        //console.log("result ", result);
    });
}

function getMessage (id, req, res) {
    Message.findById(id, function(err, post){

        if (err) return next(err);
        res.json(post);
    });
}

app.post('/auth/register', function (req, res) {
    console.log("REGISTER........" , req.body.data);
    var newUser = new User(req.body.data);
   //
    User.findOne({username: req.body.data.username}, function (err, user) {

        console.log("........req.body.username.......", req.body.data.username);
        console.log("........test0.......", user);
        //if (err) throw err;

        if (user) {
            console.log("........test0.......");
            res.send({error: "User has already registred. Please, choose another username.", status: 'found'});
        } else {
            newUser.save(function (err, resp) {

                // if user is saved

                var token = createToken (newUser);

                console.log('-----TOKEN:', token);

                // return the information including token as JSON
                res.json({
                    success: true,
                    message: 'Enjoy your token!',
                    token: token,
                    user: newUser.username,
                    data: resp
                });
                // res.send(resp);
            });
            //res.status(200);
            //res.send('200');
        }
    });


});

app.post('/auth/login', function (req, res) {

    console.log("USER WAS FOUND::   ", req.body);

    User.findOne({username: req.body.username}, function (err, user) {
        if (err) throw err;

        if (!user) {

            res.json({ success: false, message: 'Authentication failed. User not found.' });

        } else if (user) {
            // check if password matches
            if (user.password != req.body.password) {
                res.json({ success: false, message: 'Authentication failed. Wrong password.' });

            } else {
                // if user is found and password is right

                var token = createToken (user);

                // return the information including token as JSON
                res.json({
                    success: true,
                    message: 'Enjoy your token!',
                    token: token,
                    user: user.username,
                });
            }
        }

        console.log("USER WAS FOUND::   ", user);
    })
});

function createToken (user) {
    var claims = {
        user: user,
        foo: 'text',
        username:  user.username
    };
    // create a token
    var token = jwt.sign(claims, app.get('superSecret'), {
        expiresIn : 60*60*24
    });

    return token;
}

app.put('/auth/editArticle/:id', function (req, res) {
    console.log("XXXXXX TO EDIT   ", req.params.id);

    Message.findByIdAndUpdate(req.params.id, req.body, function(err, post) {
        console.log("EDITED:::::::   err", err );
        console.log("EDITED:::::::   post", post);

        if (err) return next(err);
       // res.json(post);

        getMessage(req.params.id, req, res);

    });
});

app.delete('/auth/deleteArticle/:id', function (req, res) {
    console.log("XXXXXX TO DELDETE   ", req.params.id);

    Message.findByIdAndRemove(req.params.id, function (err, post) {
        console.log('***********  ', req.params.id, '*********   ', err);
        if (err) return next(err);
        res.json(post);
    });
});

// route to show a random message (GET http://localhost:8080/api/)
//...

// route to return all users (GET http://localhost:8080/api/users)
//...

// apply the routes to our application with the prefix /api
//app.use('/api', apiRoutes);

//mongoose.connect(config.database); // connect to database
//mongodb://localhost:27017/test
//mongodb://test2:test2@ds227045.mlab.com:27045/lyrics
//mongodb://test2:test2@ds227045.mlab.com:27045/lyrics

mongoose.connect("mongodb://localhost:27017/test", function (err, db) {
    if(!err) {
        console.log("We are connected to mongo");
      //  getMessages();
    }
});

const compiler = webpack(webpackConfig);

app.use(express.static(__dirname + '/www'));

app.use(webpackDevMiddleware(compiler, {
    hot: true,
    filename: 'bundle.js',
    publicPath: '/',
    stats: {
        colors: true
    },
    historyApiFallback: true
}));

const server = app.listen(process.env.PORT || 5000, function() {
    const host = server.address().address;
    const port = server.address().port;
    console.log('Example app listening at http://%s:%s', host, port);
});