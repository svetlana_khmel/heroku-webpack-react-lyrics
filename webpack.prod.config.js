const path = require('path');
const devConfig = require('./webpack.config.js');

const config = Object.assign({}, devConfig, {
    context: path.join(__dirname, 'src'),
    entry: [
        './main.js', './styles/scss/main.scss'
    ]
});

module.exports = config;